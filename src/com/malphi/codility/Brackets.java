package com.malphi.codility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class Brackets {

	public static void main(String[] args) {
		String s = "([[]}])";
		System.out.println(solution(s));
	}
public static int solution(String S) {
	Map<String,String> matchMap = new HashMap<>();
	matchMap.put(")", "(");
	matchMap.put("]", "[");
	matchMap.put("}", "{");
	List<String> pushList = new ArrayList<>();
	pushList.add("(");
	pushList.add("{");
	pushList.add("[");
	Stack<String> stack = new Stack<>();
	for (int i = 0; i < S.length(); i++) {
		String c = String.valueOf(S.charAt(i));
		if(pushList.contains(c)){
			stack.push(c);
		}else{
			if(stack.size()==0){
				return 0;
			}else if(!matchMap.get(c).equals(stack.pop())){
				return 0;
			}
		}
	}
	if(stack.size()==0){
		return 1;
	}
	return 0;
}
}
