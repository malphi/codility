package com.malphi.codility;

public class CountDiv {

	public static void main(String[] args) {
		int a = 6, b = 11, k = 2;
		System.out.println(solution2(a, b, k));
	}

	public static int solution(int A, int B, int K) {
		int count = 0;
		for (int i = A; i <= B; i++) {
			if (i % K == 0)
				count++;
		}
		return count;
	}

	public static int solution2(int A, int B, int K) {
		int min_value = ((A + K - 1) / K) * K;
		if (min_value > B)
			return 0;
		return ((B - min_value) / K) + 1;
	}
}
