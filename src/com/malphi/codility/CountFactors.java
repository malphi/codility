package com.malphi.codility;

import java.util.ArrayList;

public class CountFactors {

	public static void main(String[] args) {
		System.out.println(solution(24));
	}

	public static int solution(int n) {
		int count = 0;
		int i = 1;
		while (i * i < n) {
			if (n % i == 0) {
				count += 2;
			}
			i++;
		}
		if (i * i == n)
			count += 1;
		return count;
	}

	public int solution(int[] A) {
		int N = A.length;

		// Find all the peaks
		ArrayList<Integer> peaks = new ArrayList<Integer>();
		for (int i = 1; i < N - 1; i++) {
			if (A[i] > A[i - 1] && A[i] > A[i + 1])
				peaks.add(i);
		}

		for (int size = 1; size <= N; size++) {
			if (N % size != 0)
				continue; // skip if non-divisible
			int find = 0;
			int groups = N / size;
			boolean ok = true;
			// Find whether every group has a peak
			for (int peakIdx : peaks) {
				if (peakIdx / size > find) {
					ok = false;
					break;
				}
				if (peakIdx / size == find)
					find++;
			}
			if (find != groups)
				ok = false;
			if (ok)
				return groups;
		}
		return 0;
	}
}
