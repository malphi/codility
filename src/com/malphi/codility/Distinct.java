package com.malphi.codility;

import java.util.HashMap;
import java.util.Map;

public class Distinct {

	public static void main(String[] args) {
		int[] a = {1,1,2,3,2,1};
		System.out.println(solution(a));
	}
	public static int solution(int[] A) {
		Map<Integer,Integer> map = new HashMap<>();
		for (int i = 0; i < A.length; i++) {
			Integer count  = map.get(A[i]);
			if(count==null){
				map.put(A[i],1);
			}else{
				map.put(A[i],count+1);
			}
		}
		for(Map.Entry<Integer, Integer> entry : map.entrySet()){
			if(entry.getValue()==1){
				return entry.getKey();
			}
		}
		return 0;
    }
}
