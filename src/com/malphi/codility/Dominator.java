package com.malphi.codility;

public class Dominator {

	public static void main(String[] args) {
		int[] a = { 3, 4, 2, 3, 3, 3, -1, 3 };
		System.out.println(solution(a));
	}

	public static int solution(int[] A) {
		int size = 0;
		int value = 0;
		for (int i = 0; i < A.length; i++) {
			if (size == 0) {
				size++;
				value = A[i];
			} else {
				if (A[i] == value)
					size++;
				else
					size--;
			}
		}
		System.out.println(size);
		int count = 0;
		int index = -1;
		for (int j = 0; j < A.length; j++) {
			if (A[j] == value) {
				count++;
				index = j;
			}
		}
		if (count <= (A.length / 2)) {
			return -1;
		}
		return index;
	}
}
