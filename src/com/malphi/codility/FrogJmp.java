package com.malphi.codility;

public class FrogJmp {

	public static void main(String[] args) {
		int x = 10, y = 21, d = 30;
		System.out.println(solution(x, y, d));
	}

	public static int solution(int X, int Y, int D) {
		int count = (Y - X) / D;
		if ((Y - X) % D > 0)
			count += 1;
		return count;
	}
}
