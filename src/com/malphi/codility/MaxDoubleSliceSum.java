package com.malphi.codility;

public class MaxDoubleSliceSum {

	public static void main(String[] args) {
		int[] a = { 3, 2, 6, -1, 4, 5, -1, 2 };
		System.out.println(solution(a));
	}

public static int solution(int[] A) {
	int[] max_ending_here = new int[A.length];
	int max_ending_here_temp = 0;
	for (int i = 1; i < A.length-1; i++) {
		max_ending_here_temp = Math.max(0,A[i]+max_ending_here_temp);
		max_ending_here[i] = max_ending_here_temp;
	}
	int[] max_beginning_here = new int[A.length];
	int max_beginning_here_temp = 0;
	for (int i = A.length-2; i >0; i--) {
		max_beginning_here_temp =Math.max(0, A[i]+max_beginning_here_temp);
		max_beginning_here[i] = max_beginning_here_temp;
	}
	int max_double_slice = 0;
	for (int i = 0; i < A.length-2; i++) {
		max_double_slice = Math.max(max_double_slice, 
				max_ending_here[i] + max_beginning_here[i+2] );
	}
	return max_double_slice;
}
}
