package com.malphi.codility;

public class MaxProfit {

	public static void main(String[] args) {

	}

	public static int maxProfit(int[] A) {
		if(A==null || A.length<2) return 0;
		int max = 0;
		int min = A[0];
		for (int i = 1; i < A.length; i++) {
			if (A[i] > min) {
				max = max > (A[i] - min) ? max : A[i] - min;
			} else {
				min = A[i];
			}
		}
		return max;
	}
}
