package com.malphi.codility;

public class MaxSliceSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a = { 3, 2, -6, 4, 0 };
		System.out.println(solution(a));
	}

public static int solution(int[] A) {
	int tmp = A[0];
	int max = A[0];
	for (int i = 1; i < A.length; i++) {
		tmp = Math.max(A[i],A[i]+tmp);
		max = Math.max(tmp, max);
	}
	return max;
}
}
