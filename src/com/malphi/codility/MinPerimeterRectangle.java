package com.malphi.codility;

public class MinPerimeterRectangle {

	public static void main(String[] args) {
		System.out.println(solution(30));
	}

	public static int solution(int n) {
		int min = Integer.MAX_VALUE;
		int i = 1;
		while (i * i <= n) {
			if (n % i == 0) {
				int p = 2 * (i + n / i);
				min = min < p ? min : p;
			}
			i++;
		}
		return min;
	}
}
