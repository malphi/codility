package com.malphi.codility;

import java.util.Arrays;

public class MissingInteger {

	public static void main(String[] args) {
		int[] a = {1,3,2,1,4,6};
		System.out.println(solution(a));
	}
	public static int solution(int[] A) {
		int min = 1;
		Arrays.sort(A);
		for (int i = 0; i < A.length; i++) {
			if(min==A[i])
				min++;
		}
		return min;
    }
}
