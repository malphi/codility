package com.malphi.codility;


public class Nesting {

	public static void main(String[] args) {
		String s = "(()(())())";
		System.out.println(solution(s));
	}
public static int solution(String S) {
	int count = 0;
	for (int i = 0; i < S.length(); i++) {
		String c = String.valueOf(S.charAt(i));
		if("(".equals(c)){
			count++;
		}else{
			count--;
			if(count<0) return 0;
		}
	}
	if(count==0) return 1;
	return 0;
}
}
