package com.malphi.codility;

import java.util.Arrays;

public class PermCheck {

	public static void main(String[] args) {
		int[] a = { 3, 3, 1 };
		Arrays.sort(a);
		System.out.println(solution2(a));
	}

	public static int solution(int[] A) {
		int temp = 0;
		int sum = 0;
		for (int i = 1; i <= A.length; i++) {
			sum += i;
		}
		for (int i = 0; i < A.length; i++) {
			temp += A[i];
		}
		return sum == temp ? 1 : 0;
	}

	public static int solution2(int[] A) {
		int max = A.length;
		for (int i = 0; i < A.length; i++) {
			if (A[i] > max)
				return 0;
		}
		return 1;
	}

	public int solution3(int[] A) {
		int[] count = new int[A.length];
		for (int i = 0; i < A.length; i++) {
			int x = A[i] - 1;
			if (x >= A.length)
				return 0;
			int check = count[x];
			if (check > 0)
				return 0;
			count[x] = 1;
		}
		return 1;
	}
}
