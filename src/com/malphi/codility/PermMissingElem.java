package com.malphi.codility;

public class PermMissingElem {

	public static void main(String[] args) {
		int[] a = { 2, 3, 1, 5 };
		System.out.println(solution(a));
	}

	public static int solution(int[] A) {
		int sum = 0;
		int total = 0;
		for (int i = 0; i < A.length; i++) {
			sum += A[i];
			total += i+1;
		}
		int result = total + A.length + 1 - sum;
		return result;
	}
}
